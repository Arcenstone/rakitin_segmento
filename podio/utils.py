# coding=utf-8
from collections import deque
import json
from simplejson import JSONDecodeError
import time
import requests

from django.conf import settings

from logs.models import ApiLogs


MEDIAPLAN_APP_ID = "18450600"
MEDIAPLAN_TOKEN = "2c85c5cbf0ba4604a3fea973aa4edb90"

CAMPAIGN_APP_ID = "18450602"
CAMPAIGN_APP_TOKEN = "eb1316fab48d41f59a6c73e4365b5ca3"


class PodioApiExeption(Exception):
    pass


class RateLimit:
    """custom implementation of rate limit on deque"""
    def __init__(self, allowed_requests, seconds):
        self.allowed_requests = allowed_requests
        self.seconds = seconds
        self.made_requests = deque()

    def __reload(self):
        t = time.time()
        while len(self.made_requests) > 0 and self.made_requests[0] < t:
            self.made_requests.popleft()

    def add_request(self):
        self.made_requests.append(time.time() + self.seconds)

    def request_available(self):
        self.__reload()
        return len(self.made_requests) < self.allowed_requests

RATE_LIMIT = RateLimit(*settings.PODIO_RATE_LIMITS)


class PodioApi:

    def __init__(self,
                 client_id=settings.PODIO_CLIENT_ID, client_secret=settings.PODIO_SECREPT_KEY,
                 redirect_uri="localhost", limits=(RATE_LIMIT,)):

        self.client_id = client_id
        self.redirect_uri = redirect_uri
        self.client_secret = client_secret
        self.limits = limits

        # oauth block save auth token as self.token
        data = {
            "grant_type": "password",
            "username": "tomarovk.irill@gmail.com",
            "password": "8G7IuS6nOHz0",
            "client_id": self.client_id,
            "redirect_uri": self.redirect_uri,
            "client_secret": self.client_secret,
        }
        self.token = self.base_request("oauth/token", method="POST",
                                       content_type="text/html", **data)['access_token']

    def can_make_request(self):
        for lim in self.limits:
            if not lim.request_available():
                return False
        return True

    def base_request(self, url, method="GET", to_json=True, content_type='application/json', **kwargs):
        """ base html request
        :param url:
        :param method: http method "GET" or "POST"
        :param to_json: true: trying to return json else: raw request
        :param content_type: html content-type
        :param kwargs: request params or body
        :return: request details or Request
        """
        try:
            header = {"Authorization": "OAuth2 {}".format(self.token)}
        except AttributeError:  # we have no token yet when authorizate
            header = {}
        data = {}
        for k, v in kwargs.items():
            if v is not None:
                data[k] = v

        url = ''.join([settings.PODIO_ENDPOINT_URL, '/' if not url.startswith('/') else "", url])

        if self.can_make_request():

            if method == "GET":
                r = requests.get(url, params=data, headers=header)
            elif method == "POST":
                if content_type == 'application/json':
                    header['content-type'] = content_type
                    data = json.dumps(data)
                    r = requests.post(url, data=data, headers=header)
                else:
                    r = requests.post(url, data=data, headers=header)
            else:
                raise NotImplementedError("ZZzzz")
        else:
            raise PodioApiExeption("Out of rate limit")

        for lim in self.limits:
            lim.add_request()

        if to_json:
            try:
                result = r.json()
            except JSONDecodeError:
                result = r.content
            finally:
                _token = getattr(self, "token", '')
                log = ApiLogs(event="podio_request", url=url, token=_token, responce=result,
                params=str(data), status_code=r.status_code)
                log.save()
        else:
            result = requests.Request(method, url, header, None, data)

        return result

    def get_app(self, app_id, **kw):
        return self.base_request("app/{app_id}".format(app_id=app_id), **kw)

    def get_app_field(self, field_id, app_id, **kw):

        return self.base_request("/app/{app_id}/field/{field_id}".
                                 format(app_id=app_id, field_id=field_id), **kw)

    def filter_items(self, app_id, **kw):
        return self.base_request("/item/app/{app_id}/filter/".format(app_id=app_id), method="POST", **kw)


def pretty_print_post(req):
    """
    formatting post for "prepared" requerst.
    """
    return ('{}\n{}\n{}\n\n{}'.format(
        '-----------START-----------',
        req.method + ' ' + req.url,
        '\n'.join('{}: {}'.format(k, v) for k, v in req.headers.items()),
        req.body,
    ))
