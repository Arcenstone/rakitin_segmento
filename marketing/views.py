# coding=utf-8
from datetime import datetime, timedelta
from collections import Counter

from django.shortcuts import render
from django.utils.safestring import mark_safe

from podio.utils import PodioApi, CAMPAIGN_APP_ID, MEDIAPLAN_APP_ID


import logging
log = logging.getLogger(__name__)

DATETIME_MASK = '%Y-%m-%d %H:%M:%S'

def frontpage(request):

    podio = PodioApi()

    now = datetime.now()
    custom_filter_for_mediaplan = {
        145398640: 4,  # status Confirmed
        145398642: {"from": (now - timedelta(days=90)).strftime(DATETIME_MASK),
                    "to": now.strftime(DATETIME_MASK)}  # last 90 days
    }

    mediaplans = podio.filter_items(MEDIAPLAN_APP_ID, filters=custom_filter_for_mediaplan)

    # create list of items where (app_id, account_manager_name)
    mediaplans = [([field['values'][0]['value'] for field in app['fields']
                    if field['label'] == 'Account Manager'][0],
                   (app['app_item_id']))

                  for app in mediaplans['items']]


    custom_filter_for_campaign = {
        145399436: 1,  # status active
        145398475: 2,  # category desktop
        "app_item_id":  [i[1] for i in mediaplans]
    }

    campaigns = podio.filter_items(CAMPAIGN_APP_ID, filters=custom_filter_for_campaign)
    mediaplans_ids_with_campaigns = [app['app_item_id'] for app in campaigns['items']]

    total_plans = Counter([i[0] for i in mediaplans])
    converted_plans = Counter([i[0] for i in mediaplans if i[1] in mediaplans_ids_with_campaigns])

    managers_data = [{"name": mark_safe(manager),
                      "total_plans": total_plans[manager],
                      "converted_plans": converted_plans[manager],
                      "kpi": round(converted_plans[manager] / total_plans[manager], 2)}

                     for manager in total_plans]

    return render(request, "base.html", {"managers_data": managers_data})



