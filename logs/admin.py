from django.contrib import admin

from .models import ApiLogs

@admin.register(ApiLogs)
class ApiLogsAdmin(admin.ModelAdmin):

    list_display = ("creation_ts", "url", "status_code")
