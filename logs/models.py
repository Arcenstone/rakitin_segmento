from django.db import models

EVENTS = (("podio_request", "Podio"),
          )


class ApiLogs(models.Model):

    event = models.CharField("Event type", max_length=50, choices=EVENTS)
    url = models.URLField("Endpoint", max_length=1000)
    token = models.CharField("Auth token", max_length=200)
    params = models.CharField("Request parameters", max_length=1000)
    responce = models.CharField("Responce", max_length=10000)
    status_code = models.IntegerField("Status code")
    creation_ts = models.DateField("Creation time", auto_now_add=True)

    def __str__(self):
        return "{}: {} {}".format(self.creation_ts, self.url, self.status_code)






